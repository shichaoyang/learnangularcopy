'use strict';

/**
 * @ngdoc function
 * @name myProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myProjectApp
 */
angular.module('myProjectApp')
    .controller('AddCtrl',
        function($scope, $stateParams, $state,
            groupsService, learnersService) {

            $scope.newVegetable = {};
            if ($stateParams.learnerid) {
                $scope.learnerId = $stateParams.learnerid;
                learnersService.find($scope.learnerId).then(function(data) {
                    // console.log(data.LearnerName);
                    $scope.newVegetable.LearnerName = data.LearnerName;
                    $scope.newVegetable.GroupId = data.GroupId;
                });
            }
            groupsService.get().success(function(data) {
                $scope.groups = data.groupList.group;
            });
            var onSubmitSuccess = function(data) {
                $state.go('list');
            };


            $scope.submit = function() {
                //console.log(data);
                var userData = $scope.newVegetable;
                if ($stateParams.learnerid) {
                    return learnersService.update($stateParams.learnerid, userData).then(onSubmitSuccess);
                }
                return learnersService.create(userData).then(onSubmitSuccess);
            };

        });