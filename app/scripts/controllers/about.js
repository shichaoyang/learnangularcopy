'use strict';

/**
 * @ngdoc function
 * @name myProjectApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the myProjectApp
 */
 
app.controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
