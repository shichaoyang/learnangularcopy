angular.module('learnangular.services', []).factory('groupsService', function($http) {

  var learnEndPoint = 'http://192.168.50.137:8080/simple-service-webapp/webapi';

  return {
    get: function() {
      return $http({
        url: learnEndPoint + '/groups',
        method: 'GET'
      });
    }
  }
});