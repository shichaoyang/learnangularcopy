angular.module('learnangular.services').service('learnersService',function( $q, Restangular, $http) {

  var find = function(id) {
    return Restangular.one('webapi/learners', id).get();
  };

  var create = function(learn) {
    return Restangular.all('webapi/learners').post(learn);
  };

  var all = function(options) {
    options = options || {};
    // var defaultParams = {type: 'learners', size: 500, sort: 'name'};
    // var requestParams = angular.extend(defaultParams, options);
    var requestParams = options;
    return Restangular.all('webapi').customGET('learners', requestParams);
  };

  var update = function(uid,learn) {
    var rest= Restangular.all('webapi').one('learners',uid);

    // .then(
    //   function(results)
    //   {

    //   }).put();
    console.log(rest);
    rest.LearnerName=learn.LearnerName;
    rest.GroupId=learn.GroupId;
    return rest.put();
  };
  
  var deleteLearn =function(id){
    return Restangular.one('webapi/learners',id).remove();
  };

  return {
    all: all,
    create: create,
    find: find,
    update: update,
    deleteLearn: deleteLearn
  };
});